from project.engine import KijijiApi
from project.engine import KijijiApiException
from project.core import post_creator
from project.utils import GaryUtils as utils
import xmltodict
import os
import time

username=""

password=""

gary = utils()

def app(username, password):
    openKijiji = KijijiApi()
    user_id, token = openKijiji.login(username, password)
    display_name = openKijiji.get_profile(user_id, token)
    print("[+] Logged in as: " + username)
    try:
        ads = gary.read_da_logs()
        for ad in ads:
            ad_id = ad.split(" ")
            for idc in ad_id:
                print("[*] Deleting ad: " + str(idc))
                try:
                    openKijiji.delete_ad(user_id, token, str(idc))
                except KijijiApiException as e:
                    print("[*] Bad exception: " + str(e))
    except KijijiApiException as e:
        print("[*] No active ads. " + str(e))

    gary.clear_logs()

    for i in range(738, 756):
        try:
            ad_id = post_creator(openKijiji, user_id, token, str(i), user_id, username)
            gary.write_to_logs(str(ad_id) + " ")
            time.sleep(1)
        except KijijiApiException as e:
            print("[*] An error occured: " + str(e))


if __name__ == "__main__":
    print("[*] A small automation tool for Kijiji and Daniel. [*]")
    app(username, password)
    print("[*] Done. [*]")










