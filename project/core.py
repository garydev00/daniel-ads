import xmltodict
import os
import configparser

# Services - Skilled Trades 2022-06-21
# <option value="738">Appliance Repair &amp; Installation</option>
# <option value="739">Brick, Masonry &amp; Concrete</option>
# <option value="740">Carpentry, Crown Moulding &amp; Trimwork</option>
# <option value="741">Drywall &amp; Stucco Removal</option>
# <option value="742">Electrician</option>
# <option value="743">Excavation, Demolition &amp; Waterproofing</option>
# <option value="744">Fence, Deck, Railing &amp; Siding</option>
# <option value="745">Flooring</option>
# <option value="746">Garage Door</option>
# <option value="747">Heating, Ventilation &amp; Air Conditioning</option>
# <option value="748">Insulation</option>
# <option value="749">Interlock, Paving &amp; Driveways</option>
# <option value="750">Lawn, Tree Maintenance &amp; Eavestrough</option>
# <option value="759">Painters &amp; Painting</option>
# <option value="751">Phone, Network, Cable &amp; Home-wiring</option>
# <option value="752">Plumbing</option
# <option value="753">Renovations, General Contracting &amp; Handyman</option>
# <option value="754">Roofing</option>
# <option value="755">Snow Removal &amp; Property Maintenance</option>
# <option value="756">Welding</option>
# <option value="757">Windows &amp; Doors</option>
# <option value="758">Other</option></select>

dir_path = os.path.dirname(os.path.realpath(__file__))
dir = os.path.dirname(dir_path)

def post_creator(kijiji_api, user_id, token, cat_id, account_id, email):    
    images = upload_image(kijiji_api)
    config = configparser.ConfigParser()
    config.read(dir + '/posts/ad.conf')
    description = open(dir + '/posts/description.gary', 'r').read()

    payload = {
        'ad:ad': 
    {
        '@xmlns:ad': 'http://www.ebayclassifiedsgroup.com/schema/ad/v1', 
        '@xmlns:cat': 'http://www.ebayclassifiedsgroup.com/schema/category/v1', 
        '@xmlns:loc': 'http://www.ebayclassifiedsgroup.com/schema/location/v1', 
        '@xmlns:attr': 'http://www.ebayclassifiedsgroup.com/schema/attribute/v1', 
        '@xmlns:types': 'http://www.ebayclassifiedsgroup.com/schema/types/v1', 
        '@xmlns:pic': 'http://www.ebayclassifiedsgroup.com/schema/picture/v1', 
        '@xmlns:vid': 'http://www.ebayclassifiedsgroup.com/schema/video/v1', 
        '@xmlns:user': 'http://www.ebayclassifiedsgroup.com/schema/user/v1', 
        '@xmlns:feature': 'http://www.ebayclassifiedsgroup.com/schema/feature/v1', 
        '@id': '', 
        'cat:category': {'@id': cat_id}, 
        'loc:locations': {'loc:location': {'@id': '1700006'}}, 
        'ad:ad-type': {'ad:value': 'OFFERED'}, 
        'ad:title': config['KIJIJI']['AD_TITLE'], 
        'ad:description': description, 
        'ad:price': {'types:price-type': {'types:value': 'FREE'}}, 
        'ad:account-id': account_id, 
        'ad:email': email, 
        'ad:poster-contact-email': email, 
        'ad:phone': config['KIJIJI']['AD_PHONENUMBER'], 
        'ad:ad-address': {
            'types:radius': 400, 
            'types:latitude': 44.4001, 
            'types:longitude': -79.6663, 
            'types:full-address': config['KIJIJI']['AD_FULLADDRESS'], 
            'types:zip-code': config['KIJIJI']['AD_ZIP']
            }, 
        'ad:visible-on-map': 'true', 
        'attr:attributes': {'attr:attribute': [{'@localized-label': '', '@name': 'payment', 'attr:value': 'cashaccepted'}]}, 
        'pic:pictures': images,
        'vid:videos': None, 
        'ad:adSlots': None, 
        'ad:listing-tags': None
    }}
    xml_payload = xmltodict.unparse(payload, short_empty_elements=True)
    print("[!] XML PAYLOAD INITIALIZED FOR CAT ID: " + cat_id)
    with open(dir + "/posts/post.xml", "w") as f:
        f.write(xml_payload)
        f.close()

    ad_id = kijiji_api.post_ad(user_id, token, xml_payload)
    return ad_id
    
def upload_image(kijiji_api):
    data = {}
    payload = {'pic:picture': []}
    for file in os.listdir(dir + "/posts/images"):
        link = kijiji_api.upload_image(open(dir + "/posts/images/" + file, "rb"))
        payload['pic:picture'].append({'pic:link': {'@rel': 'saved', '@href': link}})
        
    return payload if len(payload['pic:picture']) else {}