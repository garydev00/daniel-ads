import os

dir_path = os.path.dirname(os.path.realpath(__file__))
dir = os.path.dirname(dir_path)

class GaryUtils:
    def __init__(self):
        print("GaryUtils Initialized")

    def read_da_logs(self):
        current_ads = open(dir +"/posts/ads.log", "r")
        ads = current_ads.readlines()
        current_ads.close()
        return ads

    def write_to_logs(self, message):
        current_ads = open(dir + "/posts/ads.log", "a")
        current_ads.write(message)
        current_ads.close()

    def clear_logs(self):
        clear_ads = open(dir + "/posts/ads.log", "w")
        clear_ads.write("")
        clear_ads.close()