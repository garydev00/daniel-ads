# Auto ads

A small piece of software to automate ad posting and customer interactions, and Daniel's life.

**_Currently posting services are limited. Highly recommend purchasing a kijiji monthly subscription to upgrade posting capabilities._**

# Installation

to get started execute the command

```
pip3 install -r requirements.txt
```

locate both username and password inside main.py and replace the variable with your login credentials

```
username="test"
password="password"
```

then, run the script.

```
python3 main.py
```

# Configure AD

fill out required ad description inside "/posts/description.gary" and save

configure /posts/ads.conf to post specific data

example.

```
[KIJIJI]
AD_TITLE="30 year PROFESSIONAL Contractors. Free ESTIMATE."
AD_LOCATION="1700006"
AD_ZIP="L4M1A2"
AD_FULLADDRESS="1 Dunlop St E"
AD_PHONENUMBER="6476956128"
```

currently the location needs to be set manually
location id "170006" is for Barrie Ontario

---

All images inside "/posts/images" will be included inside the advertisement.

# Future updates

work to build a location system based off API data.
